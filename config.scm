(use-modules (gnu) (gnu system nss) (gnu services xorg))
(use-service-modules desktop)
(use-package-modules certs display-managers avahi fonts xorg gnome)

(define bepo-evdev
  "Section \"InputClass\"
  Identifier \"evdev keyboard catchall\"
  Driver \"evdev\"
  MatchIsKeyboard \"on\"
  Option \"xkb_layout\" \"fr\"
  Option \"xkb_variant\" \"bepo\"
  EndSection")

(operating-system
  (host-name "antelope")
  (timezone "Europe/Paris")
  (locale "fr_FR.utf8")

  (bootloader (grub-configuration (target "/dev/sda")))

  (file-systems (cons (file-system
                        (device (file-system-label "guixsd"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  
  (users (cons (user-account
                 (name "toto")
                 (group "users")
                 (supplementary-groups '("wheel" "netdev" "audio" "video"))
                 (home-directory "/home/toto"))
               %base-user-accounts))

  (packages (cons* nss-certs
                   gvfs
                   avahi
                   xorg-server xf86-input-evdev
                   %base-packages))

  (services
    (cons* (console-keymap-service "fr-bepo")
           (gnome-desktop-service)
           (xfce-desktop-service)
           (modify-services 
             %desktop-services
             (slim-service-type 
               config =>
               (slim-configuration
                 (inherit config)
                 (startx (xorg-start-command
                           #:configuration-file
                           (xorg-configuration-file
                             #:extra-config
                             (list bepo-evdev)))))))))

  (name-service-switch %mdns-host-lookup-nss))

